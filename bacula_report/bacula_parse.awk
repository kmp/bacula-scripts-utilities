# For reading Bacula log: generate HTML output, highlighting successful
# completions (green), warnings (yellow), errors (red); de-highlight other
# log lines, and don't include Bacula reports (lines beginning with blank).

# Formatting functions...

function htmlError(line) {
    return "<span style='color:#ff1010'>" line "</span>"
}
function htmlNormal(line) {
    return "<span style='color:#C0C0C0'>" line "</span><br />"
}
function htmlReport(line) {
    return "<span style='color:#808080'>" line "</span><br />"
}
function htmlStart(line) {
    return "<span style='color:#0000ff'>" line "</span><br />"
}
function htmlOK(line) {
    return "<span style='color:#00ff00'>" line "</span>"
}
function htmlWarning(line) {
    return "<span style='color:#ff9900'>" line "</span><br />"
}

/^  Termination/ {print htmlReport($0); next}
/^ /{next}
/^$/{next}
/JobId [[:digit:]]+: Error.*:$/ {print htmlError($0); next}
/JobId [[:digit:]]+: .*:$/ {print htmlOK($0); next}
/JobId [[:digit:]]+: Warning:.*/ {print htmlWarning($0); next}
/JobId [[:digit:]]+: Start / {print htmlStart($0); next}
/^.*:*$/ {print htmlNormal($0); next}
