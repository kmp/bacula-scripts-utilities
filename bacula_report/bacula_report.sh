#!/bin/bash
SUFFIX=""
LOGFILE="/var/log/bacula.log"
MESSAGES="/var/log/messages"

MAILTO="inbox@KMPeterson.com"
MAILFROM="root@kmpeterson.net"
EXEC_DIR=${0%/*}

DEBUG=
if [ "$1" = "" ]
then REPORT_DATE="yesterday"
else REPORT_DATE=$1 
fi

# Strings for grepping dates in log files... 'B'=Bacula log entries, 'L'=
# entries in system message log.
YESTERDAYB=`date --date="$REPORT_DATE" +"%d-%b"`
YESTERDAYL=`date --date="$REPORT_DATE" +"%b.?%e "`

# If /var/log/messages was already logrotate'd, look there instead of current
TODAY_LOGFILE=`date  +"/var/log/messages-%Y%m%d"`
if [ -e $TODAY_LOGFILE ]
then MESSAGES=$TODAY_LOGFILE
fi
# Ditto for /var/log/bacula.log
TODAY_LOGFILE=`date  +"/var/log/bacula.log-%Y%m%d"`
if [ -e $TODAY_LOGFILE ]
then LOGFILE=$TODAY_LOGFILE
fi

TMP=`/bin/mktemp -t bacreport.XXXXXXXX.html`

echo "To:      $MAILTO" > $TMP
echo "From:    $MAILFROM" >> $TMP
echo "Date:   " `date --rfc-2822` >> $TMP
echo "Subject: Bacula Report for $REPORT_DATE" >> $TMP
(
cat <<EOF
MIME-Version: 1.0
Content-Transfer-Encoding: 7bit
Content-Type: text/html; charset="iso-8859-1"

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<HTML><HEAD><STYLE type='text/css'> p {font-family: 'DejaVu Sans Mono', monospace}</STYLE></HEAD><BODY><P>
EOF
) >> $TMP

if [ $DEBUG ]
then echo "Searching log file $MESSAGES for $YESTERDAYL"
fi
echo "<H3>Messages data</H3><H5>Summary from <code>$MESSAGES$SUFFIX</code></H5><P>" >> $TMP
egrep "^$YESTERDAYL" $MESSAGES$SUFFIX | awk -f $EXEC_DIR/bacula_messages_parse.awk >> $TMP
echo "</P><H3>Bacula log data</H3><H5>Summary from <code>$LOGFILE</code></H5><P>" >> $TMP
egrep ^$YESTERDAYB $LOGFILE | awk -f $EXEC_DIR/bacula_parse.awk >> $TMP

echo "</P></BODY></HTML>" >> $TMP

if [ $DEBUG ]
then
    echo "Debug output from $TMP"
    cat $TMP
else
    cat $TMP | /usr/sbin/sendmail $MAILTO && rm -f $TMP
fi
rm -f $TMP
